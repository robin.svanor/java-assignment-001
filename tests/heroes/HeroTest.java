package heroes;

import noroff.assignments.heroes.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HeroTest {

    // When a Mage is created it should be level 1 and test for leveling Mage to level 2
    @Test
    public void getLevel_mageInstantiated_shouldSetMageToLevel1() {
        // Arrange
        Mage mage = new Mage("Mage");
        int expected = 1;

        // Act
        int actual = mage.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_levelUpMage_shouldSetMageLevelTo2() {
        //Arrange
        Mage mage = new Mage("Mage");
        mage.levelUp();
        int expected = 2;

        // Act
        int actual = mage.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    // When a Ranger is created it should be level 1 and test for leveling Ranger to level 2
    @Test
    public void getLevel_rangerInstantiated_shouldSetRangerToLevel1() {
        // Arrange
        Ranger ranger = new Ranger("Ranger");
        int expected = 1;

        // Act
        int actual = ranger.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_levelUpRanger_shouldSetRangerLevelTo2() {
        //Arrange
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp();
        int expected = 2;

        // Act
        int actual = ranger.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    // When a Rogue is created it should be level 1 and test for leveling Rogue to level 2
    @Test
    public void getLevel_rogueInstantiated_shouldSetRogueToLevel1() {
        // Arrange
        Rogue rogue = new Rogue("Rogue");
        int expected = 1;

        // Act
        int actual = rogue.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_levelUpRogue_shouldSetRogueLevelTo2() {
        //Arrange
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp();
        int expected = 2;

        // Act
        int actual = rogue.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    // When a Warrior is created it should be level 1 and test for leveling Warrior to level 2
    @Test
    public void getLevel_warriorInstantiated_shouldSetWarriorToLevel1() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        int expected = 1;

        // Act
        int actual = warrior.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_levelUpWarrior_shouldSetWarriorLevelTo2() {
        //Arrange
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp();
        int expected = 2;

        // Act
        int actual = warrior.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    // When Heroes are created they should be created with correct attributes depending on what class they are
    @Test
    public void primaryAttributes_mageInstantiated_shouldSetPrimaryAttributesToCorrectValuesForMage() {
        // Arrange
        Mage mage = new Mage("Mage");
        int strength = 1;
        int dexterity = 1;
        int intelligence = 8;

        // Act
        int actualStrength = mage.getPrimaryAttributes().getStrength();
        int actualDexterity = mage.getPrimaryAttributes().getDexterity();
        int actualIntelligence = mage.getPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void primaryAttributes_rangerInstantiated_shouldSetPrimaryAttributesToCorrectValuesForRanger() {
        // Arrange
        Ranger ranger = new Ranger("Ranger");
        int strength = 1;
        int dexterity = 7;
        int intelligence = 1;

        // Act
        int actualStrength = ranger.getPrimaryAttributes().getStrength();
        int actualDexterity = ranger.getPrimaryAttributes().getDexterity();
        int actualIntelligence = ranger.getPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void primaryAttributes_rogueInstantiated_shouldSetPrimaryAttributesToCorrectValuesForRogue() {
        // Arrange
        Rogue rogue = new Rogue("Rogue");
        int strength = 2;
        int dexterity = 6;
        int intelligence = 1;

        // Act
        int actualStrength = rogue.getPrimaryAttributes().getStrength();
        int actualDexterity = rogue.getPrimaryAttributes().getDexterity();
        int actualIntelligence = rogue.getPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void primaryAttributes_warriorInstantiated_shouldSetPrimaryAttributesToCorrectValuesForWarrior() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        int strength = 5;
        int dexterity = 2;
        int intelligence = 1;

        // Act
        int actualStrength = warrior.getPrimaryAttributes().getStrength();
        int actualDexterity = warrior.getPrimaryAttributes().getDexterity();
        int actualIntelligence = warrior.getPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }


    // When a Hero is leveled up they gain certain attributes depending on what class they are
    @Test
    public void totalPrimaryAttributes_levelUpMage_shouldAddCorrectAttributesWhenMageLevelsUp() {
        // Arrange
        Mage mage = new Mage("Mage");
        mage.levelUp();
        int strength = 2;
        int dexterity = 2;
        int intelligence = 13;

        // Act
        int actualStrength = mage.getTotalPrimaryAttributes().getStrength();
        int actualDexterity = mage.getTotalPrimaryAttributes().getDexterity();
        int actualIntelligence = mage.getTotalPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void totalPrimaryAttributes_levelUpRanger_shouldAddCorrectAttributesWhenRangerLevelsUp() {
        // Arrange
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp();
        int strength = 2;
        int dexterity = 12;
        int intelligence = 2;

        // Act
        int actualStrength = ranger.getTotalPrimaryAttributes().getStrength();
        int actualDexterity = ranger.getTotalPrimaryAttributes().getDexterity();
        int actualIntelligence = ranger.getTotalPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void totalPrimaryAttributes_levelUpRogue_shouldAddCorrectAttributesWhenRogueLevelsUp() {
        // Arrange
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp();
        int strength = 3;
        int dexterity = 10;
        int intelligence = 2;

        // Act
        int actualStrength = rogue.getTotalPrimaryAttributes().getStrength();
        int actualDexterity = rogue.getTotalPrimaryAttributes().getDexterity();
        int actualIntelligence = rogue.getTotalPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }

    @Test
    public void totalPrimaryAttributes_levelUpWarrior_shouldAddCorrectAttributesWhenWarriorLevelsUp() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp();
        int strength = 8;
        int dexterity = 4;
        int intelligence = 2;

        // Act
        int actualStrength = warrior.getTotalPrimaryAttributes().getStrength();
        int actualDexterity = warrior.getTotalPrimaryAttributes().getDexterity();
        int actualIntelligence = warrior.getTotalPrimaryAttributes().getIntelligence();

        // Assert
        assertEquals(strength, actualStrength);
        assertEquals(dexterity, actualDexterity);
        assertEquals(intelligence, actualIntelligence);
    }
}
