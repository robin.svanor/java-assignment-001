package items;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.*;
import noroff.assignments.exceptions.*;
import noroff.assignments.heroes.Warrior;
import noroff.assignments.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ItemTest {

    // Test for if a Hero tries to equip an armor piece that is to high level for them
    @Test
    public void equipItem_warriorIsToLowLevelForArmor_shouldThrowInvalidArmorException() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Armor testPlate = new Armor("Common PlateBody Armor", 2, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
        String expectedMessage = "Armor is to high level for you! This armor requires you to be level 2!";

        // Act
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.equipItem(testPlate));
        String actualMessage = exception.getMessage();

        // Assert
        assertEquals(expectedMessage, actualMessage);
    }


    // Test for if a Hero tries to equip a weapon piece that is to high level for them
    @Test
    public void equipItem_warriorIsToLowLevelForWeapon_shouldThrowInvalidWeaponException() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Weapon testWeapon = new Weapon("Common Axe", 2, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        String expectedMessage = "Weapon is to high level for you! This weapon requires you to be level 2!";

        // Act
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.equipItem(testWeapon));
        String actualMessage = exception.getMessage();

        // Assert
        assertEquals(expectedMessage, actualMessage);
    }


    // Test for if a Hero tries to equip wrong weapon type
    @Test
    public void equipItem_warriorEquipWrongWeaponType_shouldThrowInvalidWeaponException() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Weapon testBow = new Weapon("Common Bow", 1, Slot.WEAPON, WeaponType.BOW, 12, 0.8);
        String expectedMessage = "You cant equip BOW. Warrior can only equip AXE, HAMMER or SWORD!";

        // Act
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.equipItem(testBow));
        String actualMessage = exception.getMessage();

        // Assert
        assertEquals(expectedMessage, actualMessage);
    }


    // Test for if a Hero tries to wear wrong armor type
    @Test
    public void equipItem_warriorEquipWrongArmorType_shouldThrowInvalidArmorException() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Armor testClothHead = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, ArmorType.CLOTH, new PrimaryAttributes(0, 0, 5));
        String expectedMessage = "You cant wear CLOTH. Warrior can only wear MAIL or PLATE!";

        // Act
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.equipItem(testClothHead));
        String actualMessage = exception.getMessage();

        // Assert
        assertEquals(expectedMessage, actualMessage);
    }


    // Test for if a Hero tries to equip a valid weapon type
    @Test
    public void equipItem_warriorEquipValidWeaponType_shouldReturnTrue() throws InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Weapon testAxe = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        boolean expected = true;

        // Act
        boolean actual = warrior.equipItem(testAxe);

        // Assert
        assertEquals(expected, actual);
    }


    // Test for if a Hero tries to wear a valid armor type
    @Test
    public void equipItem_warriorEquipValidArmorType_shouldReturnTrue() throws InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Armor testPlate = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
        boolean expected = true;

        // Act
        boolean actual = warrior.equipItem(testPlate);

        // Assert
        assertEquals(expected, actual);
    }


    // Test for calculating a Hero's DPS without a weapon
    @Test
    public void getHeroDPS_warriorInstantiated_shouldCalculateWarriorDPS() {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        float expected = 1 * (1 + (5 / 100f));

        // Act
        float actual = warrior.getHeroDPS();

        // Assert
        assertEquals(expected, actual);
    }


    // Test for calculating a Hero's DPS with a valid weapon equipped
    @Test
    public void getHeroDPS_warriorInstantiated_shouldCalculateWarriorDPSWithValidWeaponEquipped() throws InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Weapon testAxe = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        warrior.equipItem(testAxe);
        float expected = (7 * 1.1f) * (1 + (5 / 100f));

        // Act
        float actual = warrior.getHeroDPS();

        // Assert
        assertEquals(expected, actual);
    }


    // Test for calculating a Hero's DPS with valid weapon and armor equipped
    @Test
    public void getHeroDPS_warriorInstantiated_shouldCalculateWarriorDPSWithValidWeaponAndArmorEquipped() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        Weapon testAxe = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        Armor testPlate = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
        warrior.equipItem(testAxe);
        warrior.equipItem(testPlate);
        float expected = (7 * 1.1f) * (1 + ((5 + 1) / 100f));

        // Act
        float actual = warrior.getHeroDPS();

        // Assert
        assertEquals(expected, actual);
    }
}
