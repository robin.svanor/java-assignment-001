# Assignment 1 - RPG Characters


## Description
This assignment is a Java console application. The purpose of this project is to get familiarized with OOP.


## Dependencies
 - Java JDK 17
 - JUnit 5


## Install
You need JDK to run this program


## Author
Robin Svanor