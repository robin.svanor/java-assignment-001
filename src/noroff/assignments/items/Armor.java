package noroff.assignments.items;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.*;

public class Armor extends Item{
    // Properties for the Armor class
    public ArmorType armorType;
    public PrimaryAttributes primaryAttributes;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }
}
