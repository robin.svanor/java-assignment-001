package noroff.assignments.items;

import noroff.assignments.enums.Slot;

public abstract class Item {
    // Properties for the Item class
    public String name;
    public int requiredLevel;
    public Slot slot;


    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}


