package noroff.assignments.items;

import noroff.assignments.enums.Slot;
import noroff.assignments.enums.WeaponType;

public class Weapon extends Item{
    // Properties for the Weapon class
    public WeaponType weaponType;
    public double damage;
    public double attacksPerSecond;

    public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, double damage, double attacksPerSecond) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
    }

    // Method for calculating DPS
    public double getDamagePerSecond() {
        return damage * attacksPerSecond;
    }

    // Getters and setters
    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttacksPerSecond() {
        return attacksPerSecond;
    }

    // to string
    @Override
    public String toString() {
        return "weapon{ " + "\n" +
                "weaponType= " + weaponType + "\n" +
                ", damage= " + damage + "\n" +
                ", attacksPerSecond= " + attacksPerSecond + "\n" +
                ", damagePerSecond= " + getDamagePerSecond() + "\n" +
                " }";
    }
}
