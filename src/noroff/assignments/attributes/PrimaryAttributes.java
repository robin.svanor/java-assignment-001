package noroff.assignments.attributes;

public class PrimaryAttributes {
    public int strength;
    public int dexterity;
    public int intelligence;

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // Getters and setters
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    // to string
    @Override
    public String toString() {
        return "primaryAttributes{ " + "\n" +
                "strength= " + strength + "\n" +
                ", dexterity= " + dexterity + "\n" +
                ", intelligence= " + intelligence + "\n" +
                " }";
    }
}
