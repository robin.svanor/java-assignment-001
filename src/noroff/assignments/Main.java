package noroff.assignments;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.ArmorType;
import noroff.assignments.enums.Slot;
import noroff.assignments.enums.WeaponType;
import noroff.assignments.exceptions.*;
import noroff.assignments.heroes.*;
import noroff.assignments.items.Armor;
import noroff.assignments.items.Weapon;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name to begin: ");
        String username = scanner.next();
        System.out.println("Hello " + username + "! Which class do you wish to be? ");
        System.out.println("1. Mage");
        System.out.println("2. Ranger");
        System.out.println("3. Rogue");
        System.out.println("4. Warrior");
        System.out.println("Enter the NUMBER of the Classname you would like to choose : ");
        int heroChoice = scanner.nextInt();
        System.out.println("You have chosen class number " + heroChoice + " !");
        Mage mage = new Mage(username);
        Ranger ranger = new Ranger(username);
        Rogue rogue = new Rogue(username);
        Warrior warrior = new Warrior(username);

        // Infinite loop unless user chooses to exit the program
        while (true) {
            System.out.println(username + " what do you wish to do next?");
            System.out.println("1. Level up Hero");
            System.out.println("2. Equip Armor");
            System.out.println("3. Equip Weapon");
            System.out.println("4. See Hero stats");
            System.out.println("5. Exit program");
            System.out.println("Write the number of the action you want to do :  ");
            int userAction = scanner.nextInt();
            // if user chooses to level up their Hero
            if(userAction == 1) {
                if(heroChoice == 1) {
                    mage.levelUp();
                } else if(heroChoice == 2) {
                    ranger.levelUp();
                } else if(heroChoice == 3) {
                    rogue.levelUp();
                } else if(heroChoice == 4) {
                    warrior.levelUp();
                }
            }

            // if user chooses to equip armor for their Hero
            if(userAction == 2) {
                // Armor choices for mage class
                if(heroChoice == 1) {
                    System.out.println("Mages have these Armor choices : ");
                    System.out.println("1. Cloth");
                    System.out.println("Choose the armor you want to wear (type number) : ");
                    int armorChoice = scanner.nextInt();
                    if(armorChoice == 1) {
                        Armor armor = new Armor("Cloth Leg Armor", 1, Slot.LEGS, ArmorType.CLOTH, new PrimaryAttributes(0,1,0));
                        mage.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    }
                    // Armor choices for ranger class
                } else if(heroChoice == 2) {
                    System.out.println("Rangers have these Armor choices : ");
                    System.out.println("1. Leather");
                    System.out.println("2. Mail");
                    System.out.println("Choose the armour you want to wear (type number) : ");
                    int armorChoice = scanner.nextInt();
                    if(armorChoice == 1) {
                        Armor armor = new Armor("Leather Leg Armor", 1, Slot.LEGS, ArmorType.LEATHER, new PrimaryAttributes(0,1,0));
                        ranger.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    } else if(armorChoice == 2) {
                        Armor armor = new Armor("Mail Body Armor", 1, Slot.BODY, ArmorType.MAIL, new PrimaryAttributes(1,0,0));
                        ranger.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    }
                    // Armor choices for rogue class
                } else if(heroChoice == 3) {
                    System.out.println("Rogues have these Armor choices : ");
                    System.out.println("1. Leather");
                    System.out.println("2. Mail");
                    System.out.println("Choose the armour you want to wear (type number) : ");
                    int armorChoice = scanner.nextInt();
                    if(armorChoice == 1) {
                        Armor armor = new Armor("Leather Leg Armor", 1, Slot.LEGS, ArmorType.LEATHER, new PrimaryAttributes(0,1,0));
                        rogue.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    } else if(armorChoice == 2) {
                        Armor armor = new Armor("Mail Body Armor", 1, Slot.BODY, ArmorType.MAIL, new PrimaryAttributes(1,0,0));
                        rogue.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    }
                    // Armor choices for warrior class
                } else if(heroChoice == 4) {
                    System.out.println("Warriors have these Armor choices : ");
                    System.out.println("1. Mail");
                    System.out.println("2. Plate");
                    System.out.println("Choose the armour you want to wear (type number) : ");
                    int armorChoice = scanner.nextInt();
                    if(armorChoice == 1) {
                        Armor armor = new Armor("Mail Body Armor", 1, Slot.BODY, ArmorType.MAIL, new PrimaryAttributes(1,0,0));
                        warrior.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    } else if(armorChoice == 2) {
                        Armor armor = new Armor("Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1,0,0));
                        warrior.equipItem(armor);
                        System.out.println("Equipped " + armor.name + "\n");
                    }
                }
            }

            // if user chooses to equip a weapon for their Hero
            if(userAction == 3) {
                // Weapon choices for mage class
                if(heroChoice == 1){
                    System.out.println("Mages have these Weapon choices : ");
                    System.out.println("1. Staff");
                    System.out.println("2. Wand");
                    System.out.println("Choose the weapon you want to equip (type number) : ");
                    int weaponChoice = scanner.nextInt();
                    if(weaponChoice == 1) {
                        Weapon weapon = new Weapon("Common Staff", 1, Slot.WEAPON, WeaponType.STAFF, 7, 1);
                        mage.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    } else if(weaponChoice == 2) {
                        Weapon weapon = new Weapon("Common Wand", 1, Slot.WEAPON, WeaponType.WAND, 5, 1.3);
                        mage.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    }
                    // Weapon choices for ranger class
                } else if(heroChoice == 2) {
                    System.out.println("Rangers have these Weapon choices : ");
                    System.out.println("1. Bow");
                    System.out.println("Choose the weapon you want to equip (type number) : ");
                    int weaponChoice = scanner.nextInt();
                    if(weaponChoice == 1) {
                        Weapon weapon = new Weapon("Common Bow", 1, Slot.WEAPON, WeaponType.BOW, 8, 0.8);
                        ranger.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    }
                    // Weapon choices for rogue class
                } else if(heroChoice == 3) {
                    System.out.println("Rogue have these Weapon choices : ");
                    System.out.println("1. Dagger");
                    System.out.println("2. Sword");
                    System.out.println("Choose the weapon you want to equip (type number) : ");
                    int weaponChoice = scanner.nextInt();
                    if(weaponChoice == 1) {
                        Weapon weapon = new Weapon("Common Dagger", 1, Slot.WEAPON, WeaponType.DAGGER, 9, 0.7);
                        rogue.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    } else if(weaponChoice == 2) {
                        Weapon weapon = new Weapon("Common Sword", 1, Slot.WEAPON, WeaponType.SWORD, 13, 0.5);
                        rogue.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    }
                    // Weapon choices for warrior class
                }else if(heroChoice == 4) {
                    System.out.println("Warrior have these Weapon choices : ");
                    System.out.println("1. Axe");
                    System.out.println("2. Hammer");
                    System.out.println("3. Sword");
                    System.out.println("Choose the weapon you want to equip (type number) : ");
                    int weaponChoice = scanner.nextInt();
                    if(weaponChoice == 1) {
                        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 11, 0.6);
                        warrior.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    } else if(weaponChoice == 2) {
                        Weapon weapon = new Weapon("Common Hammer", 1, Slot.WEAPON, WeaponType.HAMMER, 10, 0.7);
                        warrior.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    } else if(weaponChoice == 3) {
                        Weapon weapon = new Weapon("Common Sword", 1, Slot.WEAPON, WeaponType.SWORD, 13, 0.5);
                        warrior.equipItem(weapon);
                        System.out.println("Equipped " + weapon.name + "\n");
                    }
                }
            }

            // if user chooses to see Hero stats
            if(userAction == 4) {
                if(heroChoice == 1) {
                    System.out.println(mage + "\n");
                } else if(heroChoice == 2) {
                    System.out.println(ranger + "\n");
                } else if(heroChoice == 3) {
                    System.out.println(rogue + "\n");
                }else if(heroChoice == 4) {
                    System.out.println(warrior + "\n");
                }
            }

            // if user chooses to Exit the program
            if(userAction == 5) {
                System.out.println("Program shutting down ...... ");
                System.exit(0);
            }
        }
    }
}
