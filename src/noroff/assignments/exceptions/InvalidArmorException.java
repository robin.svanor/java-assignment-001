package noroff.assignments.exceptions;

public class InvalidArmorException extends Exception {
    private String message;

    public InvalidArmorException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "InvalidArmorException thrown: " + this.message;
    }
}
