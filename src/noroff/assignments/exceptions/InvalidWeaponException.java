package noroff.assignments.exceptions;

public class InvalidWeaponException extends Throwable {
    private String message;

    public InvalidWeaponException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String toString() {
        return "InvalidWeaponException thrown: " + this.message;
    }
}
