package noroff.assignments.heroes;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.*;
import noroff.assignments.exceptions.*;
import noroff.assignments.items.*;

import java.util.HashMap;

public class Rogue extends Hero {

    public Rogue(String name) {
        super("Rogue", new PrimaryAttributes(2, 6, 1), new PrimaryAttributes(2, 6, 1), new HashMap<>());
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.setPrimaryAttributes(1, 4, 1);
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    @Override
    public boolean equipItem(Armor armor) throws InvalidArmorException {
        // Check required level of armor and throw error if user is to low level
        if(armor.getRequiredLevel() > this.getLevel()) {
            throw new InvalidArmorException("Armor is to high level for you! " +
                    "This armor requires you to be level " + armor.getRequiredLevel() + "!");
            // Rogues can only use LEATHER or MAIL
        } else if(armor.armorType == ArmorType.LEATHER || armor.armorType == ArmorType.MAIL) {
            this.getEquipment().put(armor.slot, armor);
            return true;
        } else {
            throw new InvalidArmorException("You cant wear " + armor.armorType + ". Rogues can only wear LEATHER or MAIL!");
        }
    }

    @Override
    public boolean equipItem(Weapon weapon) throws InvalidWeaponException {
        // Check required level of weapon and throw error if user is to low level
        if(weapon.getRequiredLevel() > this.getLevel()) {
            throw new InvalidWeaponException("Weapon is to high level for you! " +
                    "This weapon requires you to be level " + weapon.getRequiredLevel() + "!");
            // Rogues can only use DAGGER or SWORD
        } else if(weapon.weaponType == WeaponType.DAGGER || weapon.weaponType == WeaponType.SWORD) {
            this.getEquipment().put(weapon.slot, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("You cant equip " + weapon.weaponType + ". Rogues can only equip DAGGER or SWORD!");
        }
    }

    @Override
    public float getHeroDPS() {
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        double weaponDPS;
        // If no weapon is equipped then weaponDPS is set to 1
        if(weapon == null) {
            weaponDPS = 1;
            // If there is a weapon it gets the DPS from the Weapon class
        } else {
            weaponDPS = weapon.getDamagePerSecond();
        }
        return (float) (weaponDPS * (1 + getTotalPrimaryAttributes().dexterity / 100f));
    }

    @Override
    public String toString() {
        return "Rogue{ " +
                "name= " + getName() +
                ", level = " + getLevel() +
                ", primaryAttributes= " + getPrimaryAttributes() +
                " }";
    }
}
