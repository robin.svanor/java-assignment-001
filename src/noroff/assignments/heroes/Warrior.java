package noroff.assignments.heroes;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.*;
import noroff.assignments.exceptions.*;
import noroff.assignments.items.*;

import java.util.HashMap;

public class Warrior extends Hero {

    public Warrior(String name) {
        super("Warrior", new PrimaryAttributes(5, 2, 1), new PrimaryAttributes(5, 2, 1), new HashMap<>());
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.setPrimaryAttributes(3, 2, 1);
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    @Override
    public boolean equipItem(Armor armor) throws InvalidArmorException {
        // Check required level of armor and throw error if user is to low level
        if(armor.getRequiredLevel() > this.getLevel()) {
            throw new InvalidArmorException("Armor is to high level for you! " +
                    "This armor requires you to be level " + armor.getRequiredLevel() + "!");
            // Warrior can only use MAIL or PLATE
        } else if(armor.armorType == ArmorType.MAIL || armor.armorType == ArmorType.PLATE) {
            this.getEquipment().put(armor.slot, armor);
            return true;
        } else {
            throw new InvalidArmorException("You cant wear " + armor.armorType + ". Warrior can only wear MAIL or PLATE!");
        }
    }

    @Override
    public boolean equipItem(Weapon weapon) throws InvalidWeaponException {
        // Check required level of weapon and throw error if user is to low level
        if(weapon.getRequiredLevel() > this.getLevel()) {
            throw new InvalidWeaponException("Weapon is to high level for you! " +
                    "This weapon requires you to be level " + weapon.getRequiredLevel() + "!");
            // Warrior can only use AXE, HAMMER or SWORD
        } else if(weapon.weaponType == WeaponType.AXE || weapon.weaponType == WeaponType.HAMMER || weapon.weaponType == WeaponType.SWORD) {
            this.getEquipment().put(weapon.slot, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("You cant equip " + weapon.weaponType + ". Warrior can only equip AXE, HAMMER or SWORD!");
        }
    }

    @Override
    public float getHeroDPS() {
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        double weaponDPS;
        // If no weapon is equipped then weaponDPS is set to 1
        if(weapon == null) {
            weaponDPS = 1;
            // If there is a weapon it gets the DPS from the Weapon class
        } else {
            weaponDPS = weapon.getDamagePerSecond();
        }
        return (float) (weaponDPS * (1 + getTotalPrimaryAttributes().strength / 100f));
    }

    @Override
    public String toString() {
        return "Warrior{ " +
                "name= " + getName() +
                ", level = " + getLevel() +
                ", primaryAttributes= " + getPrimaryAttributes() +
                " }";
    }

}
