package noroff.assignments.heroes;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.Slot;
import noroff.assignments.exceptions.*;
import noroff.assignments.items.*;

import java.util.HashMap;

// Base class for every hero that is created
public abstract class Hero {
    // Properties for the Hero class
    private String name;
    // Every Hero starts at level 1
    private int level = 1;
    protected PrimaryAttributes primaryAttributes;
    private  PrimaryAttributes totalPrimaryAttributes = new PrimaryAttributes(0, 0, 0);
    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected double increaseDamageAttributes;

    public Hero(String name, PrimaryAttributes primaryAttributes, PrimaryAttributes totalPrimaryAttributes, HashMap<Slot, Item> equipment) {
        this.name = name;
        this.primaryAttributes = primaryAttributes;
        this.totalPrimaryAttributes = totalPrimaryAttributes;
        this.equipment = new HashMap<Slot, Item>();
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.primaryAttributes.setStrength(this.primaryAttributes.getStrength() + strength);
        this.primaryAttributes.setDexterity(this.primaryAttributes.getDexterity() + dexterity);
        this.primaryAttributes.setIntelligence(this.primaryAttributes.getIntelligence() + intelligence);
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        int strength = primaryAttributes.getStrength();
        int dexterity = primaryAttributes.getDexterity();
        int intelligence = primaryAttributes.getIntelligence();

        for(Item item: equipment.values()) {
            if(item instanceof Armor) {
                strength += ((Armor) item).primaryAttributes.strength;
                dexterity += ((Armor) item).primaryAttributes.dexterity;
                intelligence += ((Armor) item).primaryAttributes.intelligence;
            }
        }
        return new PrimaryAttributes(strength, dexterity, intelligence);
    }

    public void setTotalPrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.totalPrimaryAttributes.setStrength(this.primaryAttributes.getStrength() + strength);
        this.totalPrimaryAttributes.setDexterity(this.primaryAttributes.getDexterity() + dexterity);
        this.totalPrimaryAttributes.setIntelligence(this.primaryAttributes.getIntelligence() + intelligence);
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public float getHeroDPS() {
        Weapon weapon = (Weapon) this.equipment.get(Slot.WEAPON);
        float heroDPS;
        if(weapon == null) {
            heroDPS = (float) (1 + (this.increaseDamageAttributes / 100f));
        } else {
            heroDPS = (float) (weapon.getDamagePerSecond() * (1 + this.increaseDamageAttributes / 100f));
        }
        return heroDPS;
    }

    public void levelUp() {
        this.level++;
    }

    public abstract boolean equipItem(Armor armor) throws InvalidArmorException;

    public abstract boolean equipItem(Weapon weapon) throws InvalidWeaponException;

    @Override
    public String toString() {
        return "hero{" + "\n" +
                "name= " + name + "\n" +
                ", level= " + level + "\n" +
                ", strength= " + totalPrimaryAttributes.getStrength() + "\n" +
                ", dexterity= " + totalPrimaryAttributes.getDexterity() + "\n" +
                ", intelligence= " + totalPrimaryAttributes.getIntelligence() + "\n" +
                ", DPS= " + getHeroDPS() + "\n" +
                " }";
    }
}
