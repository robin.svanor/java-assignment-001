package noroff.assignments.heroes;

import noroff.assignments.attributes.PrimaryAttributes;
import noroff.assignments.enums.*;
import noroff.assignments.exceptions.*;
import noroff.assignments.items.*;

import java.util.HashMap;

public class Mage extends Hero {

    public Mage(String name) {
        super("Mage", new PrimaryAttributes(1, 1, 8), new PrimaryAttributes(1, 1, 8), new HashMap<>());
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.setPrimaryAttributes(1, 1, 5);
        this.setTotalPrimaryAttributes(0, 0, 0);
    }

    @Override
    public boolean equipItem(Armor armor) throws InvalidArmorException {
        // Check required level of armor and throw error if user is to low level
        if(armor.getRequiredLevel() > this.getLevel()) {
            throw new InvalidArmorException("Armor is to high level for you! " +
                    "This armor requires you to be level " + armor.getRequiredLevel() + "!");
        // Mages can only use CLOTH
        } else if(armor.armorType == ArmorType.CLOTH) {
            this.getEquipment().put(armor.slot, armor);
            return true;
        } else {
            throw new InvalidArmorException("You cant wear " + armor.armorType + ". Mages can only wear CLOTH!");
        }
    }

    @Override
    public boolean equipItem(Weapon weapon) throws InvalidWeaponException {
        // Check required level of weapon and throw error if user is to low level
        if(weapon.getRequiredLevel() > this.getLevel()) {
            throw new InvalidWeaponException("Weapon is to high level for you! " +
                    "This weapon requires you to be level " + weapon.getRequiredLevel() + "!");
        // Mages can only use STAFF or Wand
        } else if(weapon.weaponType == WeaponType.STAFF || weapon.weaponType == WeaponType.WAND) {
            this.getEquipment().put(weapon.slot, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("You cant equip " + weapon.weaponType + ". Mages can only equip STAFF or WAND!");
        }
    }

    @Override
    public float getHeroDPS() {
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        double weaponDPS;
        // If no weapon is equipped then weaponDPS is set to 1
        if(weapon == null) {
            weaponDPS = 1;
        // If there is a weapon it gets the DPS from the Weapon class
        } else {
            weaponDPS = weapon.getDamagePerSecond();
        }
        return (float) (weaponDPS * (1 + getTotalPrimaryAttributes().intelligence / 100f));
    }

    @Override
    public String toString() {
        return "Mage{ " +
                "name= " + getName() +
                ", level = " + getLevel() +
                ", primaryAttributes= " + getPrimaryAttributes() +
                " }";
    }
}
