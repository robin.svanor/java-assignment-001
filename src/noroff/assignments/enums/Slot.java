package noroff.assignments.enums;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
