package noroff.assignments.enums;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
